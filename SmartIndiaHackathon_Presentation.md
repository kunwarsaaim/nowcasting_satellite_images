# Smart India Hackathon 2020

## NM374-ISRO

### Problem Statement

Nowcasting of Meteorological Satellite images using AI/ML techniques.

### Introduction

INSAT-3D and INSAT-3DR are two geostationary meteorological satellites of India having 6 channel Imager and 19 channel Sounder payloads. INSAT-3D and INSAT-3DR Imager acquire images over its footprint every 30 Min. In order to have better revisit these satellites are programmed to acquire images in staggered mode, which provides images over Indian region every 15 Min.

Nowcasting refers to forecasting for a shorter duration (3 to 6 Hrs.). Nowcasting of meteorological images will help in forecasting images with for next 3 hrs.

These Nowcasted images help synoptic meteorologists, administrators and common man for better interpretation and decision support during extreme weather events.

Machine learning (ML) provides techniques for predicting the new outcomes based on previously known results and Artificial Intelligence (AI) helps in decision making.

## Table Of Content

- Problem Statement
- Introduction
- Architecture
- Inspiration & Proposal
- Approach
- Architecture
- Model Results
- Metrics
- Data Info
- Discussions
- Further Improvement

### Link for ppt : [CLICK_HERE](https://myamuacin-my.sharepoint.com/:p:/g/personal/mhkhan13_myamu_ac_in/EQOpm125zrdOpLxsidWI1boB39D9HlEyyljl7mj0waQ3HA?e=TGDdKa)
