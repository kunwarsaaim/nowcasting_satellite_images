# NM374_Starlink

### Team Starlink :
- Kunwar Muhammad Saaim (Team Leader)
- Mohd Hozaifa Khan
- Saloni Gupta
- Pranshi Jindal
- Mohd Ammad Rehman
- Atif Bilal

## Problem Statement

Nowcasting of Meteorological Satellite Images using AI/ML

## Nowcasting

Nowcasting is a technique to forecast the weather a short period ahead. In the pre-Deep learning Era, it was done using optical flow vectors.This forecast is an extrapolation in time of known weather parameters, including those obtained by means of remote sensing, using techniques that take into account a possible evolution of the air mass.

<p align="center">
  <img src="https://gitlab.com/kunwarsaaim/nowcasting_satellite_images/-/raw/master/images/mainimg2.jpg" height = "400px">
</p>
 
## Introduction

INSAT-3D and INSAT-3DR are two geostationary meteorological satellites of India having 6 channel Imager and 19 channel Sounder payloads. INSAT-3D and INSAT-3DR Imager acquire images over its footprint every 30 Min. In order to have better revisit these satellites are programmed to acquire images in staggered mode, which provides images over Indian region every 15 Min.

Nowcasting refers to forecasting for a shorter duration (3 to 6 Hrs.). Nowcasting of meteorological images will help in forecasting images with for next 3 hrs.

These Nowcasted images help synoptic meteorologists, administrators and common man for better interpretation and decision support during extreme weather events.

Machine learning (ML) provides techniques for predicting the new outcomes based on previously known results and Artificial Intelligence (AI) helps in decision making.

### Applications

Interpretation and decision support during extreme weather events. Nowcasting can be used to forecast and monitor:

- convective storms with attendant phenomena
- locally forced precipitation events.
- local flows or circulations (breeze, foehn, low level jets, convergence lines, dry lines, boundaries, etc.)

## Our Proposal

We propose to implement an deep-learning method called Residual Recurrent-Unet or __R2Unet__ for next frame prediction. R2Unet is a CNN based Encoder-Decoder Architecture for performing Image to Image Translation.

### Inspiration

__Self-Driving Cars__: Predict next frame at 30 fps which is wonderful !

## Tech Stack
- Pytorch Framework
- geopandas
- gdal
- PIL
- Rasterio
- Scikit-image
- NumPy

## Architecture
A conv encoding and decoding units using recurrent convolutional layers (RCL) based U-Net architecture. The residual units are used with RCL for R2U-Net architecture.

### Advantages
There are several advantages of these proposed architectures for segmentation tasks:
- A residual unit helps when training deep architecture. Second, 
- Feature accumulation with recurrent residual convolutional layers ensures better feature representation for segmentation tasks. 
- It allows us to design better U-Net architecture with same number of network parameters with better performance for medical image segmentation.

## Results
<p align="center">
  <img src="https://gitlab.com/kunwarsaaim/nowcasting_satellite_images/-/raw/78fb65e4b196ff8e7d65dd4532a2f83239f466d0/images/VIS.png" height = "500px"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
  <img src="https://gitlab.com/kunwarsaaim/nowcasting_satellite_images/-/raw/78fb65e4b196ff8e7d65dd4532a2f83239f466d0/images/SWIR.png" height = "500px">
 </p>
<p>
 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; VIS  &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp; SWIR
</p>

Generate nowcasted satellite images and its animation loop for next 3 hours at an interval of 30 minutes.

## Data Information
### Trained
- May and August 2019 INSAT 3D and INSAT 3DR 6 channel MERCATOR
- Trained for 512X512 Image Crop. crop is Random OFFSET
- 6 Models for 4 channel viz. MIR, SWIR, VIS, TIR2

### Predicted
- 512X512 image at an OFFSET of (600, 600).
- 6 Day images from September 2019 (1-6).

## Other Details
 - Go through our [presentation](https://gitlab.com/kunwarsaaim/nowcasting_satellite_images/-/blob/master/SmartIndiaHackathon_Presentation.md)
