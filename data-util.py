import os
import pandas as pd
import pickle as pkl


<<<<<<< HEAD
root = './data/'

folder_names = ['insat', 'may_insat', 'sep_insat']

images = []
loc = []
for dir in folder_names:
    new = os.listdir(root + dir + '/')
    images = images + new
    loc = loc + [dir]*len(new)

data = {'name': images, 'location': loc }

dataframe = pd.DataFrame(data)
dataframe['name'].apply(lambda x: root + x + '/')
with open('./data/image_list.pkl', 'wb') as f:
    pkl.dump(dataframe, f) 
=======
def load(path):
    with open(path, 'rb') as f:
        dt = pkl.load(f)  

def transform(dataframe):
  list_ = []
  for index,row in dataframe.iterrows(): 
    list_.append('./'+row['location']+'/'+row['name'])

  return list_

def build_kmers(sequence, ksize = 16):
    kmers = []
    n_kmers = len(sequence) - ksize + 1

    for i in range(n_kmers):
        kmer = sequence[i:i + ksize]
        kmers.append(kmer)

    return kmers

def if __name__ == "__main__":
    
    root = './data/'

    folder_names = ['insat', 'may_insat', 'sep_insat']
    channels = ['MIR', 'SWIR', 'TIR1', 'TIR2', 'VIS', 'WV']

    images = dict()
    loc = []
    for dir in folder_names:
        images[dir] = sorted(os.listdir('./' + dir + '/'))  

        data = {'name':images[dir],'location':[dir]*len(images[dir])}
        dataframe = pd.DataFrame(data)
        dataframe['channel'] =  dataframe['name'].apply(lambda x: x[38:-4])
        
        for ch in channels:
        data_f = dataframe[dataframe['channel'] == ch]
        d = build_kmers(transform(data_f))
        
        with open('./data/'+dir+'_'+ch+'.pkl', 'wb') as f:
            pkl.dump(d, f)

    # dt = load(path)
>>>>>>> origin/unet
