import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn import init
from torchsummary import summary



class conv_block(nn.Module):
    def __init__(self,in_filters,out_filters):
        super(conv_block,self).__init__()

        
        self.conv1 = nn.Conv2d(in_filters,out_filters,kernel_size=3,padding=1)
        self.conv2 = nn.Conv2d(out_filters,out_filters,kernel_size=3,stride=1,padding=1)

        self.batchnorm1 = nn.BatchNorm2d(out_filters)

    def forward(self,x):
        x = F.leaky_relu(self.batchnorm1(self.conv1(x)),negative_slope=0.2)
        x = self.conv2(x)

        return x


class up_conv(nn.Module):
    def __init__(self,in_filter,out_filters):
        super(up_conv,self).__init__()

        self.up = nn.ConvTranspose2d(in_filter,out_filters,kernel_size=3,stride=2,padding=1,output_padding=1)
        self.batchnorm = nn.BatchNorm2d(out_filters)

    def forward(self,x):
        x = F.leaky_relu(self.batchnorm(self.up(x)),negative_slope=0.2)
        return x


class Recurrent_block(nn.Module):
    def __init__(self,out_filters,t=2):
        super(Recurrent_block,self).__init__()
        self.t = t
        self.ch_out = out_filters

        self.conv = nn.Conv2d(self.ch_out,self.ch_out,kernel_size=3,stride=1,padding=1)
        self.batchnorm = nn.BatchNorm2d(self.ch_out)

    def forward(self,x):
        for i in range(self.t):

            if i==0:
                x1 = F.leaky_relu(self.batchnorm(self.conv(x)),negative_slope=0.2)

            x1 = F.leaky_relu(self.batchnorm(self.conv(x+x1)),negative_slope=0.2)
        
        return x1
    
class R2CNN_block(nn.Module):
    def __init__(self,in_filter,out_filters,t=2):
        super(R2CNN_block,self).__init__()

        self.RCNN = nn.Sequential(
            Recurrent_block(out_filters,t=t),
            Recurrent_block(out_filters,t=t)
        )

        self.Conv_1x1 = nn.Conv2d(in_filter,out_filters,kernel_size=1,stride=1,padding=0)

    def forward(self,x):
        x = self.Conv_1x1(x)
        x1 = self.RCNN(x)
        return x+x1


class R2U_Net(nn.Module):
    def __init__(self,img_ch=3,output_ch=1,t=2,ch_mul=64):
        super(R2U_Net,self).__init__()

        self.R2CNN1 = R2CNN_block(img_ch,out_filters=ch_mul,t=t)
        self.R2CNN2 = R2CNN_block(ch_mul,ch_mul*2,t=t)
        self.R2CNN3 = R2CNN_block(ch_mul*2,ch_mul*4,t=t)
        self.R2CNN4 = R2CNN_block(ch_mul*4,ch_mul*8,t=t)
        self.R2CNN5 = R2CNN_block(ch_mul*8,ch_mul*16,t=t)

        self.Up5 = up_conv(ch_mul*16,ch_mul*8)
        self.Up_R2CNN5 = R2CNN_block(ch_mul*16,ch_mul*8,t=t)
        
        self.Up4 = up_conv(ch_mul*8,ch_mul*4)
        self.Up_R2CNN4 = R2CNN_block(ch_mul*8,ch_mul*4,t=t)

        self.Up3 = up_conv(ch_mul*4,ch_mul*2)
        self.Up_R2CNN3 = R2CNN_block(ch_mul*4,ch_mul*2,t=t)

        self.Up2 = up_conv(ch_mul*2,ch_mul)
        self.Up_R2CNN2 = R2CNN_block(ch_mul*2,ch_mul)

        self.Conv_1x1 = nn.Conv2d(ch_mul,output_ch,kernel_size=1,stride=1)

    def forward(self,x):
        #encoding
        x1 = self.R2CNN1(x)

        x2 = self.R2CNN2(F.max_pool2d(x1,(2,2)))

        x3 = self.R2CNN3(F.max_pool2d(x2,(2,2)))

        x4 = self.R2CNN4(F.max_pool2d(x3,(2,2)))

        x5 = self.R2CNN5(F.max_pool2d(x4,(2,2)))

        #decoding+ concat

        d5 = torch.cat([x4,self.Up5(x5)],1)
        d5 = self.Up_R2CNN5(d5)

        d4 = torch.cat([x3,self.Up4(d5)],1)
        d4 = self.Up_R2CNN4(d4)

        d3 = torch.cat([x2,self.Up3(d4)],1)
        d3 = self.Up_R2CNN3(d3)

        d2 = torch.cat([x1,self.Up2(d3)],1)
        d2 = self.Up_R2CNN2(d2)

        d1 = self.Conv_1x1(d2)

        return d1

