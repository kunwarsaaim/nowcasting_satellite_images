import torch
import torch.nn as nn
from torch.autograd import Variable
from torch.utils.data import Dataset, DataLoader
import numpy as np
import rasterio
from PIL import Image
import os
import pandas as pd
import pickle as pkl
from functools import reduce
from rasterio.windows import Window
from model import R2U_Net
import random




# def accuracy(true,pred):
# 	acc = (true == pred.argmax(1)).float().detach().cpu().numpy()
#     return float(100 * acc.sum() / len(acc)

class Sequncedataset(Dataset):
	def __init__(self, data_frame1,data_frame2):
# 		self.root_dir = root_dir
		with open(data_frame1,'rb') as f:
			self.sequence_list1 = pkl.load(f)
		with open(data_frame2,'rb') as f:
			self.sequence_list2 = pkl.load(f)
		self.sequence_list = self.sequence_list1+self.sequence_list2			
            
	def __len__(self):
		return len(self.sequence_list)

	def __getitem__(self,index):
		frame = []
		x = random.randint(600,800)
		y = random.randint(600,700)
		for i in range(16):
			img_path = os.path.join(self.sequence_list[index][i])
			with rasterio.open(img_path) as src:
				im = src.read(1, window=Window(x, y, 512, 512))
			im = np.array(im,dtype=np.float32)
			im = torch.tensor(im,dtype=torch.float32)
			frame.append(im)

		frame = torch.stack(frame,dim=0)
		frame = frame/1023
        
		return frame[:10],frame[10:]
	
class Sequncedataset_test(Dataset):
	def __init__(self, data_frame):
# 		self.root_dir = root_dir
		with open(data_frame,'rb') as f:
			self.sequence_list = pkl.load(f)
		
	def __len__(self):
		return len(self.sequence_list)

	def __getitem__(self,index):
		frame = []
		x = random.randint(600,800)
		y = random.randint(600,700)
		for i in range(16):
			img_path = os.path.join(self.sequence_list[index][i])
			with rasterio.open(img_path) as src:
				im = src.read(1, window=Window(x, y, 512, 512))
			im = np.array(im,dtype=np.float32)
			im = torch.tensor(im,dtype=torch.float32)
			frame.append(im)

		frame = torch.stack(frame,dim=0)
		frame = frame/1023
        
		return frame[:10],frame[10:]
		
def pytorch_count_params(model):
  "count number trainable parameters in a pytorch model"
  total_params = sum(reduce( lambda a, b: a*b, x.size()) for x in model.parameters())
  return total_params

model = R2U_Net(img_ch=10,output_ch=6,ch_mul=16).cuda()


print(pytorch_count_params(model))

train_dataloader = DataLoader(Sequncedataset('./data/may_insat_TIR1.pkl','./data/insat_TIR1.pkl'),batch_size=4,shuffle = True,num_workers=2)
test_dataloader = DataLoader(Sequncedataset_test('./data/sep_insat_TIR1.pkl'),batch_size=4,shuffle = True,num_workers=2)

# for i, data in enumerate(train_dataloader, 0):
#     x,y = data
#     x = Variable(x)
#     y = Variable(y)
#     print(x.size)
#     print(y.size)
#     break
criterion_class= nn.MSELoss() 
optim = torch.optim.Adam(model.parameters(),lr=0.001) #Adam optimizer
epoch=0
path_to_model = None
if path_to_model != None:
	checkpoint = torch.load(path_to_model)
	model.load_state_dict(checkpoint['model_state_dict'])
	optim.load_state_dict(checkpoint['optimizer_state_dict'])
	epoch = checkpoint['epoch']
	print('model loaded')


def train(epoch):
  running_loss = 0.0
  accuracy_batch=0.0
  train_loss = 0.0
  model.train()
  for i, data in enumerate(train_dataloader, 0):
    x,y = data
    x = x.cuda()
    y = y.cuda()
    optim.zero_grad()
    y_hat = model(x)
    loss = criterion_class(y_hat,y)
    loss.backward()
    optim.step()
    running_loss += loss.item()
    train_loss += loss.item()
    if i % 10 == 9:    # print every 200 mini-batches
        print('[%d, %5d] loss: %.8f' %
              (epoch + 1, i + 1, running_loss / 10))
        running_loss = 0.0
  print('====> Epoch: {} Average train loss: {:.6f}'.format(
          epoch+1,train_loss/(len(train_dataloader))))
  
def test(epoch):
  accuracy_batch = 0.0
  test_loss = 0.0
  model.eval()
  for i,data in enumerate(test_dataloader,0):
    x,y = data
    x = x.cuda()
    y = y.cuda()
    optim.zero_grad()
    y_hat = model(x)
    loss = criterion_class(y_hat,y)
    test_loss += loss.item()
  print('====> Epoch: {} Average test loss: {:.6f}'.format(
          epoch+1,test_loss/(len(test_dataloader))))

for epoch in range(epoch+1,25):
  train(epoch)
  test(epoch)
  torch.save({
            'epoch': epoch,
            'model_state_dict': model.state_dict(),
            'optimizer_state_dict': optim.state_dict(),
            }, 'results/TIR1_6/insat_TIR1_all'+str(epoch)+'.pth')


    
