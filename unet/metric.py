import numpy as np

from skimage.measure import compare_ssim as ssim
from skimage.metrics import mean_squared_error



def calculate_psnr(img1, img2, max_value=255):
   
    mse = np.mean((np.array(img1, dtype=np.float32) - np.array(img2, dtype=np.float32)) ** 2)
    if mse == 0:
        return 100
    return 20 * np.log10(max_value / (np.sqrt(mse)))

def calculate_ssim(img_true,img_pred):
	ssim_noise = ssim(img_true, img_pred)

